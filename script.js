// Variáveis auxiliares
x = 0
soma = 0
qnt = 0

// Função que limpa as áreas e realiza o cálculo
function calcular() {

    // Limpa a área de resultado
    document.getElementById("result").value = ""

    var valorVisor = parseFloat(document.getElementById('nota').value.replace(',','.'))
    
    // Condicionais para checar se é ou não um número, se a área está vazia ou o número é maior que 10 e, caso as condições sejam cumpridas, realizar a adição da nota no sistema
    if (isNaN(valorVisor)) {
        alert("A nota digitada é inválida, por favor, insira uma nota válida")
        document.getElementById('nota').value = ""
    } else if (valorVisor == "" || valorVisor == null || valorVisor == " " || valorVisor == "  " || valorVisor == "   " || valorVisor == "    ") {
        alert("Por favor, insira uma nota")
        document.getElementById('nota').value = ""
    } else if (valorVisor > 10) {
        alert("A maior nota possível é 10!")
        document.getElementById('nota').value = ""
    } else {
        x++
        document.getElementById('areatxt').value += "A nota " + x + " foi: " + valorVisor + "\r\n"
        soma += parseFloat(valorVisor)
        document.getElementById('nota').value = ""
        qnt++
    }

}

// Função que realiza o cálculo da média
function media() {

    document.getElementById("areatxt").value = ""
    document.getElementById("result").value = soma / qnt
    x = 0
    soma = 0
    qnt = 0

}